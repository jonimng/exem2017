<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii;

class User extends ActiveRecord implements \yii\web\IdentityInterface
{
    public $role;
    
    public static function tableName(){
        return 'user';// ����� ����� ���� 
    }
   
    /**
     * @inheritdoc
     */
    public static function findIdentity($id)// ����� ��.� �� ������ ���� �����
    {
        return self::findOne($id); // ���� ������ �� ����� ����� �������� ������ ��� ��� ����
    }
    
    public function rules() // ����� ���� ����� ����� ����
    {
        return
        [
            [['username', 'password', 'auth_Key'],'string', 'max' =>255],
            [['username', 'password'],'required'],
            [['username'], 'unique'],
            ['role', 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)// ?
    {
        throw new NotSupportedException('Not supported');

        return null;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)// �������� ���� �� ����� ������ �� �������� ������� ��� ��
    {
        return self::findOne(['username'=>$username]);// ���� �������� ����� �� ����� ����� ���� �� �������� ��������
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_Key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($auth_Key)
    {
        return $this->auth_Key === $auth_Key;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
  
  
  //test hashed password

    public function validatePassword($password)
    {
        return $this->isCorrectHash($password, $this->password);
    }
    
    private function isCorrectHash($plaintext, $hash)
    {
        return Yii::$app->security->validatePassword($plaintext, $hash);
    }
  
  
  
  
  
    //hash password before saving
    public function beforeSave($insert)
    {
        $return = parent::beforeSave($insert);

        if ($this->isAttributeChanged('password'))
            $this->password = Yii::$app->security->
                    generatePasswordHash($this->password);

        if ($this->isNewRecord)
            $this->auth_Key = Yii::$app->security->generateRandomString(32);

        return $return;
    }
    
    

   
public function afterSave($insert,$changedAttributes)
    {
        $return = parent::afterSave($insert, $changedAttributes);

        $auth = Yii::$app->authManager;
        $roleName = $this->role; 
        $role = $auth->getRole($roleName);
        if (isset($role))
        {
            if (\Yii::$app->authManager->getRolesByUser($this->id) == null){
                $auth->assign($role, $this->id);
            } else {
                $db = \Yii::$app->db;
                $db->createCommand()->delete('auth_assignment',
                    ['user_id' => $this->id])->execute();
                $auth->assign($role, $this->id);
            }
        }

        return $return;
    }

    

    //create fullname pseuodo field ����� ����� -
    public function getUserName()
    {
        return $this->username;
    }

    
    
    
//A method to get an array of all users models/User
    public static function getUsers()
    {
        $users = ArrayHelper::
                    map(self::find()->all(), 'id', 'username');// ���� �� ������ �� �� ��.� ��� �������
        return $users;                      
    }

    
    
//A method to get an array of all roles
    
public function getRoleTypes()
    {
        /** @var \yii\rbac\DbManager $authManager */
        $roller = Yii::$app->get('authManager')->getRoles();

        foreach ($roller as $item)
        {
           $role[$item->name] = $item->name;

        }


        return $role;
    }
    
    



}