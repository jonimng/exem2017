<?php
namespace app\rbac;

use yii\rbac\Rule;
use Yii; 

class OwnPostRule extends Rule
{
	public $name = 'ownPostRule';

	public function execute($user, $item, $params)
	{
		if (!Yii::$app->user->isGuest) {
			return isset($params['post']) ? $params['post']->author == $post : false;
		}
		return false;
	}
}