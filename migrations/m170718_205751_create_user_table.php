<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m170718_205751_create_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull(),
			'auth_Key' => $this->string()->notNull(),
			'password' => $this->string()->notNull(),
               ]);
    }
    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('user');
    }
}