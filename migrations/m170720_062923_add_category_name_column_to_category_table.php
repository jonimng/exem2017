<?php

use yii\db\Migration;

/**
 * Handles adding category_name to table `category`.
 */
class m170720_062923_add_category_name_column_to_category_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('category', 'category_name', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('category', 'category_name');
    }
}
