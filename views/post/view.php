<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Post */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Posts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
			[ // the authour name 
	
				'label' => $model->attributeLabels()['author'],
				'format' => 'html',
				'value' => Html::a($model->userAuthor->username, 
					['user/view', 'id' => $model->userAuthor->id]),
			],
            'body:ntext',
          
			[ // the status name 
				'label' => $model->attributeLabels()['status'],
                'format' => 'html',
				'value' => function($model){
                    return $model->statusItem->status_name;},
			],
            [ // the category name of the lead
				'label' => $model->attributeLabels()['category'],
				'format' => 'html',
                'value' => function($model){
                    return $model->categoryItem->category_name;},	
			],
            [ // post created at
				'label' => $model->attributeLabels()['created_at'],
				'value' => date('d/m/Y H:i:s', $model->created_at)
			],
            [ // post updated at
				'label' => $model->attributeLabels()['updated_at'],
				'value' => date('d/m/Y H:i:s', $model->updated_at)
			],		
            [ // post created by
				'label' => $model->attributeLabels()['created_by'],
				'value' => isset($model->createdBy->username) ? $model->createdBy->username : 'No one!',	
			],
            [ // post updated by
				'label' => $model->attributeLabels()['updated_by'],
				'value' => isset($model->updateddBy->username) ? $model->updateddBy->username : 'No one!',	
			],
        ],
    ]) ?>

</div>
